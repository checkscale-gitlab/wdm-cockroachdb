#!/bin/bash

# Remove relevant containers, volumes and images
if [ ! "docker container ls -aq --filter \"name=wdm-cockroachdb\"" ]; then
  docker container stop $(docker container ls -aq --filter "name=wdm-cockroachdb")
  docker container rm -f --volumes $(docker container ls -aq --filter "name=wdm-cockroachdb")
  docker image prune -f
fi

[ ! "docker network ls --filter \"name=wdm-cockroach_db_default\"" ] && docker network rm wdm-cockroach_db_default

# Build docker compose
docker-compose -f docker-compose.prod.yml build --no-cache
