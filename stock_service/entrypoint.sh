#!/bin/sh

if [ "$DATABASE" = "cockroachdb" ]; then
  echo "Waiting for CockroachDB..."

  while ! nc -z $SQL_HOST $SQL_PORT; do
    sleep 5
  done

  echo "CockroacdDB started"
fi

echo "Waiting for RabbitMQ..."
while ! nc -z $RABBITMQ_HOST $RABBITMQ_PORT; do
  sleep 5
done
echo "RabbitMQ started"

python manage.py flush --no-input
python manage.py makemigrations
python manage.py migrate

exec "$@"
