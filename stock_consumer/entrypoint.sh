#!/bin/sh

echo "Waiting for Stock service..."
while ! nc -z $STOCK_SERVICE_HOST $STOCK_SERVICE_PORT; do
  sleep 5
done
echo "Stock service started"

exec "$@"
