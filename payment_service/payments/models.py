from django.db import models


class User(models.Model):
    id = models.BigAutoField(primary_key=True)
    credit = models.FloatField(default=0.0)


class Payment(models.Model):
    id = models.BigAutoField(primary_key=True)
    is_paid = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
