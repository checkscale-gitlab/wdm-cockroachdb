import json
import os

import django
import pika

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

from orders.models import Item, User


def on_items_created(channel, method_frame, header_frame, body):
    data = json.loads(body)
    try:
        item = Item(id=data['id'], cost=data['cost'])
        item.save()

    except Exception as e:
        print(e)


def on_user_created(channel, method_frame, header_frame, body):
    data = json.loads(body)
    try:
        user = User(id=data['id'])
        user.save()

    except Exception as e:
        print(e)


credentials = pika.PlainCredentials(
    username=os.environ.get('RABBITMQ_USERNAME'),
    password=os.environ.get('RABBITMQ_PASSWORD')
)

params = pika.ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST'),
    port=os.environ.get('RABBITMQ_PORT'),
    credentials=credentials,
    heartbeat=600,
    blocked_connection_timeout=300
)

connection = pika.BlockingConnection(params)

channel = connection.channel()
channel.queue_declare(queue='order_items_created')
channel.basic_consume(queue='order_items_created', on_message_callback=on_items_created, auto_ack=True)
channel.queue_declare(queue='order_user_created')
channel.basic_consume(queue='order_user_created', on_message_callback=on_user_created, auto_ack=True)

try:
    print('Started consuming')
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()

connection.close()
